PARALLEL EXPECTATION MAXIMIZATION ALGORITHM

This C++ code implements a new parallel expectation maximization algorithm.

You will need to install G++ in linux environment.

===========================================================================
 			QUICK START OF PEM ALGORITHM
===========================================================================
Please Choose PEM1(synchronization) or PEM2(asynchronous) first.
usage:
To make file:
	make
To run:
	./pem convert train_file binary_train_file
	./pem convert test_file binary_test_file
	./pem train [Options] binary_train_file binary_test_file model
	(you can find the simple example in quickstart)
To clean:
	make clean


===========================================================================
				PEM COMMAND 
===========================================================================
**********************************************************************
1.		   	PRE-TREATMENT
**********************************************************************
you should convert both training and test data into binary file

usage: pem convert file_name binary_file_name

****************************
* Format of Input Data:		
*    docId   wordId  count
*    docId   wordId  count
*    ... ...
*    docId   wordId  count
****************************

**********************************************************************
2.			   TRAINING
**********************************************************************
You can train the pem model by the training set

usage: pem train [options] binary_train_file mode
options:
	-k <dimensions>:	set the number of dimensions (default 10) 
	-t <iterations>:	set the number of iterations (default 100) 
	-s <number of threads>:
						set the number of threads (default 4)  
	-pn <partition number>:
						set the partition number of the documents (default 30)
	-rrate	<the rate of residual>:
						set the rate of residual (default 0.2)
	-blk <blocks>:		set the number of blocks for parallel PEM (default 2s x 2s) 
						For example, if you want 3x4 blocks, then use '-blk 3x4' 
	-alpha				set the number of parameter(alpha) (default 0.01)
	-beta				set the number of parameter(beta) (default 0.01)
	--rand-shuffle, 
	--no-rand-shuffle:	enable/disable random shuffle (default disabled)
						This options should be used when the data is imbalanced.
	--tr-perp,
	--no-tr-perp:		enable/disable show perplexity on training data (default disabled)
						This option shows the estimated Perplexity on training data. 
						It also slows down the training procedure.
	--te-perp
	--no-te-perp:		enable / disable show perplexity on test data (default enabled)
						This option shows the estimated Perplexity on test data. It also slows
						down the test procedure.

You can get any parameter you need in Model::write(tools/pem.cpp) by add 
	a write function.
For example:
	Phi, Theta, phitot or any other parameters.

=============================================================================
The total program will run like this:
=============================================================================
Converting ../dataset/cora... done. 0.03
Writing cora.tr.bin... done. 0.00
Converting ../dataset/cora... done. 0.03
Writing cora.te.bin... done. 0.00
Initializing model...done. 0.02
Starts!
DocsID  iter    tr_time    te_time    te_perp
0       10         0.60
0       20         1.16
(Test Set)                    2.67    398.606
1       10         1.65
1       20         2.12
(Test Set)                    5.08    289.376
2       10         2.57
2       20         2.98
(Test Set)                    7.58    242.027
3       10         3.41
3       20         3.81
(Test Set)                   10.00    214.818
4       10         4.27
4       20         4.67
(Test Set)                   12.14    196.425
Writing model...done. 0.03

