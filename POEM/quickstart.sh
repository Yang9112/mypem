#!/bin/sh

set -e

./poem convert ../dataset/cora cora.tr.bin
./poem convert ../dataset/cora cora.te.bin
./poem train  --no-tr-perp -rrate 0.1 -pn 5 -t 20 -s 12 -k 1000 cora.tr.bin cora.te.bin model
