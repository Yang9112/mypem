#include "poem.h"
#include "omp.h"

oModel::oModel() {}
oModel::oModel(char *path) { read(path); }

void oModel::initialize(Matrix *Tr, int fg) {
	if(!fg) printf("Initializing model..."); fflush(stdout);
	Clock clock; clock.tic();
	
	nr_ds = Tr->nr_ds;
	nr_ws = Tr->nr_ws;
	nr_rs = Tr->nr_rs;
	dim_off = dim%4? (dim/4)*4+4 : dim;
	showfg = fg;

	WBETA = BETA*nr_ws; JALPHA = ALPHA*dim;

	//initialzation the phi & theta
	if(fg) {
		for(int ix = 0; ix < nr_gwbs*nr_gdbs; ix++)
			delete mymu[ix];
		for(int px = 0; px < nr_ws; px++)
			for(int dx = 0; dx < dim_off; dx++)
				Phi2[px*dim_off + dx] = Phi[dx*dim_off + dx];
		
		delete[] Theta;
		delete[] Theta2;
		delete[] thetad;
		delete[] rdk;
		delete[] idk;
	}
	else {
		Phi = new double[nr_ws*dim_off];
		Phi2 = new double[nr_ws*dim_off];
		phiw = new double[nr_ws];
		phitot = new double[dim_off];
		phitot2 = new double[dim_off];
		mymu = new double*[nr_gwbs*nr_gdbs];

		for(int px = 0; px < nr_ws; px++) {
			phiw[px] = 0; 
			for(int dx = 0; dx < dim_off; dx++) {
				Phi[px*dim_off + dx] = 0;
				Phi2[px*dim_off + dx] = 0;
			}
		}
		for(int ix = 0; ix < dim_off; ix++) {
			phitot[ix] = 0; phitot2[ix] = 0;
		}
	}
		
	xitot = 0.0;
	rrate = 1;
	Theta = new double[nr_ds*dim_off];
	Theta2 = new double[nr_ds*dim_off];
	thetad = new double[nr_ds];
	rdk = new double[nr_ds*dim_off];
	idk = new int[nr_ds*dim_off];

	for(int qx = 0; qx < nr_ds; qx++) {
		thetad[qx] = 0;
		for(int dx = 0; dx < dim_off; dx++) {
			Theta[qx*dim_off + dx] = 0;
			Theta2[qx*dim_off + dx] = 0;
			rdk[qx*dim_off + dx] = 0;
			idk[qx*dim_off + dx] = dx;
		}
	}

	if(!fg) printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void oModel::initData(GridMatrix *TrG) {
	Node *rn, *r;
	double *curmu, rating;

	for(int mx = 0; mx < nr_gwbs*nr_gdbs; mx++) {
		rn = TrG->GMS[mx]->M;
		mymu[mx] = new double[TrG->GMS[mx]->nr_rs * dim_off];
		curmu = mymu[mx];
		for(int dx = 0; dx < TrG->GMS[mx]->nr_rs; dx++)
			for(int ddx = 0; ddx < dim_off; ddx++)
				curmu[dx*dim_off + ddx] = 0;
		
		for(int dx = 0; dx < TrG->GMS[mx]->nr_rs; dx++) {
			r = rn; rn++;
			rating = r->rate;
			thetad[r->did] += rating;
			phiw[r->wid] += rating;
			xitot += rating;
			int topic = (int) (dim*drand48());
			Phi[r->wid*dim_off + topic] += rating;
			Theta[r->did*dim_off + topic] += rating;
			phitot[topic] += rating;
			curmu[dx*dim_off + topic] = 1.0;
		}
	}
}

void oModel::initialize_te(Matrix *Te) {
	//printf("Initializing test model..."); fflush(stdout);
	Clock clock; clock.tic();

	int mx, dx;
	nr_ds = Te->nr_ds;
	nr_rs = Te->nr_rs;
	WBETA = nr_ws*BETA;
	JALPHA = dim*ALPHA;
	xitot = 0.0;

	delete [] Theta;
	delete [] thetad;
	for(int mx = 0; mx < nr_gwbs*nr_gdbs; mx++)
		delete mymu[mx];

	Theta = new double[nr_ds*dim_off];
	mymu = new double*[nr_gwbs*nr_gdbs];
	thetad = new double[nr_ds];
	phitot2 = new double[dim_off];

	for(int qx = 0; qx < nr_ds; qx++) {
		thetad[qx] = 0;
		for(dx = 0; dx < dim_off; dx++) {
			Theta[qx*dim_off + dx] = 0;
		}
	}

	for(mx = 0; mx < dim_off; mx++) {
		phitot2 = 0;;
	}

	//printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void oModel::initData_te(GridMatrix *TrG) {
	Node *rn, *r;
	double *curmu, rating;

	for(int mx = 0; mx < nr_gwbs*nr_gdbs; mx++) {
		rn = TrG->GMS[mx]->M;
		mymu[mx] = new double[TrG->GMS[mx]->nr_rs * dim_off];
		curmu = mymu[mx];
		for(int dx = 0; dx < TrG->GMS[mx]->nr_rs; dx++)
			for(int ddx = 0; ddx < dim_off; ddx++)
				curmu[dx*dim_off + ddx] = 0;

		for(int dx = 0; dx < TrG->GMS[mx]->nr_rs; dx++) {
			r = rn; rn++;
			rating = r->rate;
			thetad[r->did] += rating;
			xitot += rating;
			int topic = (int) (dim*drand48());
			Theta[r->did*dim_off + topic] += rating;
			curmu[dx*dim_off + topic] = 1.0;
		}
	}
}

void oModel::updateParameter() {
	int ratenum = 0;
	rrate = rrate0;

	if((int)(dim_off*rrate0) >= dim)
		ratenum = dim;
	else
		ratenum = (int)(dim*rrate) + 1;	

	#pragma omp parallel for num_threads(nr_thrs)
	for(int tx = 0; tx < nr_thrs; tx++) {
		for(int qx = tx; qx < nr_ds; qx += nr_thrs) {
			psort(rdk + qx*dim_off, idk + qx*dim_off, 0, dim-1, ratenum);
			//std::sort(idk + qx*dim_off, idk + qx*dim_off + ratenum - 1);
		}
	}
	
	for(int qx = 0; qx < nr_ds; qx++)
		for(int dx = 0; dx < (int)(dim*rrate); dx++)
			rdk[qx*dim_off + idk[qx*dim_off + dx]] = 0;

	for(int mx = 0; mx < dim_off; mx++)
		phitot[mx] = 0;
	
	for(int px = 0; px < nr_ws; px++)
		for(int dx = 0; dx < dim; dx++)
			phitot[dx] += Phi[px*dim_off + dx];
}

void oModel::updateParameter_te() {
	for(int qx = 0; qx < nr_ds; qx++) {
		for(int dx = 0; dx < dim_off; dx++) {
			Theta[qx*dim_off + dx] = Theta2[qx*dim_off + dx];
			Theta2[qx*dim_off + dx] = 0;
		}
	}
}

void oModel::read(char *path) {
	printf("Reading model..."); fflush(stdout);
	Clock clock; clock.tic();
	FILE *f = fopen(path,"rb"); if(!f) exit_file_error(path);
	read_meta(f);

	Phi = new double[nr_ws*dim_off];
	fread(Phi, sizeof(double), nr_ws*dim_off, f);
	phiw = new double[nr_ws];
	fread(phiw, sizeof(double), nr_ws, f);
	phitot = new double[dim_off];
	fread(phitot, sizeof(double), dim_off, f);

	if(en_rand_shuffle) {
		map_df = new int[nr_ds]; 
		map_db = new int[nr_ds];
		map_wf = new int[nr_ws];
		map_wb = new int[nr_ws]; 
		fread(map_df, sizeof(int), nr_ds, f);
		fread(map_db, sizeof(int), nr_ds, f);
		fread(map_wf, sizeof(int), nr_ws, f);
		fread(map_wb, sizeof(int), nr_ws, f);
	}
	fclose(f);
	printf("done. %.2f\n",clock.toc()); fflush(stdout);
}

void oModel::write(char *path) {
	printf("Writing model..."); fflush(stdout);
	Clock clock; clock.tic();
	FILE *f = fopen(path, "wb"); 
	if(!f) exit_file_error(path);
	
	float ver = (float)MODELVER;
	int file_type = MODEL;
	
	fwrite(&file_type,sizeof(int),1,f);
	fwrite(&ver,sizeof(float),1,f);
	fwrite(this,sizeof(Model),1,f);
	fwrite(Phi,sizeof(double),nr_ws*dim_off,f);
	fwrite(phiw, sizeof(double), nr_ws, f);
	fwrite(phitot, sizeof(double), dim_off, f);
	if(en_rand_shuffle) {
		fwrite(map_df, sizeof(int), nr_ds, f);
		fwrite(map_db, sizeof(int), nr_ds, f);
		fwrite(map_wf, sizeof(int), nr_ws, f);
		fwrite(map_wb, sizeof(int), nr_ws, f);
	}
	fclose(f);
	printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void oModel::shuffle() {
	double *Theta1 = new double[nr_ds*dim_off];
	double *Phi1 = new double[nr_ws*dim_off];
	double *thetad1 = new double[nr_ds];
	double *phiw1 = new double[nr_ws];

	for(int px=0; px<nr_ds; px++) {
		std::copy(&Theta[px*dim_off],&Theta[px*dim_off + dim_off],&Theta1[map_df[px] * dim_off]);
		thetad1[map_df[px]] = thetad[px];
	}
	for(int qx=0; qx<nr_ws; qx++) {
		std::copy(&Phi[qx*dim_off],&Phi[qx*dim_off + dim_off],&Phi1[map_wf[qx] * dim_off]);
		phiw1[map_wf[qx]] = phiw[qx];
	}

	delete [] Phi; delete [] Theta; 
	delete [] phiw; delete [] thetad;

	Phi = Phi1; Theta = Theta1;
	phiw = phiw1; thetad = thetad1;
}

void oModel::inv_shuffle() {
	double *Theta1 = new double[nr_ds*dim_off]; 
	double *Phi1 = new double[nr_ws*dim_off];
	double *thetad1 = new double[nr_ds];
	double *phiw1 = new double[nr_ws];

	for(int px=0; px<nr_ds; px++) {
		std::copy(&Theta[px*dim_off],&Theta[px*dim_off + dim_off],&Theta1[map_db[px]*dim_off]);
		thetad1[map_db[px]] = thetad[px];
	}
	for(int qx=0; qx<nr_ws; qx++) {
		std::copy(&Phi[qx*dim_off],&Phi[qx*dim_off + dim_off],&Phi1[map_wb[qx]*dim_off]);
		phiw1[map_wb[qx]] = phiw[qx];
	}

	delete [] Phi; delete [] Theta;
	delete [] phiw; delete [] thetad;

	Phi = Phi1; Theta = Theta1; 
	phiw = phiw1; thetad = thetad1;
}

double oModel::get_perp(Matrix *R) {
	double perp = 0.0, mutot;
	long int nr_rs = R->nr_rs;
	Node *rn = R->M;
 
	for(long mx = 0; mx < nr_rs; mx++, rn++) {
		float xi = rn->rate;
		mutot = 0.0;
		for(int dx = 0; dx < dim; dx++) {
			mutot += (Phi[rn->wid*dim_off + dx] + BETA)/(phitot[dx] + WBETA)*
				(Theta[rn->did*dim_off + dx] + ALPHA)/(thetad[rn->did] + JALPHA);
		}
		perp -= (log(mutot)*xi);
	}

	return perp;
}

double oModel::get_deltaPhi() {
	double deltaPhitot = 0.0;

	for(int mx = 0; mx < nr_ws; mx++)
		for(int dx = 0; dx < dim_off; dx++)
			deltaPhitot += pow(Phi2[mx*dim_off + dx] - Phi[mx*dim_off + dx],2);

	return deltaPhitot/xitot;	
}

int oModel::partition(double *x, int *indx, int left, int right, int pivotIndx) 
{
	double pivotValue = x[indx[pivotIndx]];
	int i, tmp, storeIndx = left;

	/* move pivot to end */
	tmp = indx[pivotIndx];
	indx[pivotIndx] = indx[right];
	indx[right] = tmp;

	for (i=left; i<right; i++) {
		if (x[indx[i]] > pivotValue) {
			tmp = indx[i];
			indx[i] = indx[storeIndx];
			indx[storeIndx] = tmp;
			storeIndx++;
		}
	}

	/* move pivot to storeIndx */
	tmp = indx[storeIndx];
	indx[storeIndx] = indx[right];
	indx[right] = tmp;

	return storeIndx;
}

/* *index should be in the range [0, length(x)] */
void oModel::psort(double *x, int *indx, int left, int right, int k)
{
	int pivotIndx, pivotNewIndx;

	if (right > left) {
		pivotIndx = (int) ((right + left)/2);
		pivotNewIndx = partition(x, indx, left, right, pivotIndx);
		if (pivotNewIndx > left + k) {
			psort(x, indx, left, pivotNewIndx - 1, k);
		}
		if (pivotNewIndx < left + k) {
			psort(x, indx, pivotNewIndx + 1, right, k + left - pivotNewIndx - 1); 
		}
	}
}

void oModel::deleteMap() {
	delete [] map_df; delete [] map_db; delete [] map_wf; delete [] map_wb;
}

oModel::~oModel() { 
	delete [] Phi; delete [] Theta;	delete [] phitot;
	delete [] thetad; delete [] phiw;
	delete [] Phi2; delete [] Theta2; delete [] phitot2;
	delete [] mu;
}

	
