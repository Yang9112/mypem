#include "pem.h"

struct oModel:public Model {
	double *Phi, *Theta, *Phi2, *Theta2, *phiw, *thetad;
	double **mymu;
	double rrate, rrate0, *rdk;
	int partition_num, *idk;

	int showfg;

	oModel();
	oModel(char *src);
	void read(char *path);
	void write(char *path);
	void shuffle();
	void inv_shuffle();
	double get_perp(Matrix *R);
	double get_deltaPhi();
	void initialize(Matrix *Tr, int fg = 0);
	void initData(GridMatrix *TrG);
	void initialize_te(Matrix *Tr);
	void initData_te(GridMatrix *TrG);
	void updateParameter();
	void updateParameter_te();
	int partition(double *x, int *indx, int left, int right, int pivotIndx);
	void psort(double *x, int *indx, int left, int right, int k);
	void deleteMap();
	~oModel();
};

void predict(oModel *model, Monitor *monitor, char *te_path);
