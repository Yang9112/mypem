#include "poem.h"

struct GridMatrix;

struct TrainOption {
	char *tr_path, *te_path, *model_path;
	TrainOption(int argc, char **argv, oModel *model, Monitor *monitor);	
	static void exit_train();
	~TrainOption();
};

TrainOption::TrainOption(int argc, char **argv, oModel *model, Monitor *monitor) {

	model->dim=10, model->nr_thrs=4, model->iter=100, model->nr_gdbs=0, model->nr_gwbs=0;
	model->en_rand_shuffle=false;

	model->rrate0 = 0.2;
	model->BETA = 0.01; model->ALPHA = 0.01;
	model->partition_num = 30;
	monitor->en_show_tr_perp=false;
	monitor->en_show_te_perp=true;
	
	int i;
	for(i=2; i<argc; i++) {
		if(argv[i][0]!='-') break;
		if(i+1>=argc) exit_train();
		if(!strcmp(argv[i], "-k")) {
			model->dim = atoi(argv[++i]);
			if(model->dim<=0) { fprintf(stderr,"dimensions should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-t")) {
			model->iter = atoi(argv[++i]);
			if(model->iter<=0) { fprintf(stderr,"iterations should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-s")) {
			model->nr_thrs = atoi(argv[++i]);
			if(model->nr_thrs<=0) { fprintf(stderr,"number of threads should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-pn")) {
			model->partition_num = atoi(argv[++i]);
			if(model->nr_thrs<1) { fprintf(stderr,"the split size of documents should>= 1\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-rrate")) {
			model->rrate0 = atof(argv[++i]);
			if(model->rrate0 > 1 || model->rrate0 <= 0) { fprintf(stderr,"number of residual rate should < 1 and > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-blk")) {
			char *p = strtok(argv[++i],"x");
			model->nr_gdbs = atoi(p);

			p = strtok(NULL,"x");
			model->nr_gwbs = atoi(p);

			if(model->nr_gdbs<=0 || model->nr_gwbs<=0) { fprintf(stderr,"number of blocks should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-alpha")) {
			model->ALPHA = atof(argv[++i]);
			if(model->iter<=0) { fprintf(stderr,"the number of parameter(alpha) should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-beta")) {
			model->BETA = atof(argv[++i]);
			if(model->iter<=0) { fprintf(stderr,"the number of parameter(beta) should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "--rand-shuffle")) model->en_rand_shuffle = true;
		else if(!strcmp(argv[i], "--no-rand-shuffle")) model->en_rand_shuffle = false;
		else if(!strcmp(argv[i], "--tr-perp")) monitor->en_show_tr_perp = true;
		else if(!strcmp(argv[i], "--no-tr-perp")) monitor->en_show_tr_perp = false;
		else if(!strcmp(argv[i], "--te-perp")) monitor->en_show_te_perp = true;
		else if(!strcmp(argv[i], "--no-te-perp")) monitor->en_show_te_perp = false;
		else { fprintf(stderr,"Invalid option: %s\n", argv[i]); exit_train(); }
	}

	if(model->nr_gdbs==0) (model->nr_gdbs) = 2*(model->nr_thrs);
	if(model->nr_gwbs==0) (model->nr_gwbs) = 2*(model->nr_thrs);

	if(i>=argc) exit_train();

	tr_path = argv[i++]; 
	te_path = argv[i++];

	if(i<argc) {
		model_path = new char[strlen(argv[i])+1];
		sprintf(model_path,"%s",argv[i]);
	}
	else {
		char *p = strrchr(argv[i-1],'/');
		if(p==NULL)
			p = argv[i-1];
		else
			++p;
		model_path = new char[strlen(p)+7];
		sprintf(model_path,"%s.model",p);
	}
} 

void TrainOption::exit_train() {
	printf(
	"usage: pem train [options] binary_train_file model\n"
	"\n"
	"options:\n" 
	"-k <dimensions>: set the number of dimensions (default 40)\n" 
	"-t <iterations>: set the number of iterations (default 40)\n" 
	"-s <number of threads>: set the number of threads (default 4)\n"  
	"-pn <partition number>: set the partition number of the documents (default 30)\n"  
	"-rrate <the rate of residual>: set the rate of residual (default 0.2)\n" 
	"-blk <blocks>: set the number of blocks for parallel PEM (default 2s x 2s)\n" 
	"	For example, if you want 3x4 blocks, then use '-blk 3x4'\n" 
	"-alpha: set the number of parameter(alpha) (default 0.01)\n"
	"-beta: set the number of parameter(beta) (default 0.01)\n"
	"--rand-shuffle --no-rand-shuffle: enable / disable random suffle (default disabled)\n"
	"	This options should be used when the data is imbalanced.\n"
	"--tr-perp --no-tr-perp: enable / disable show perplexity on training data (default disabled)\n"
	"	This option shows the estimated Perplexity on training data. It also slows down the training procedure.\n"
	"--te-perp --no-te-perp: enable / disable show perplexity on training data (default enabled)\n"
	"	This option shows the estimated Perplexity on test data. It also slows down the test procedure.\n"
	); 
	exit(1);
}
TrainOption::~TrainOption() { delete [] model_path; }

void emStep(GridMatrix *TrG, oModel *model, Scheduler *scheduler, int tid) {
	double *const Phi = model->Phi, *const Theta = model->Theta;
	double **const mu = model->mymu;
	const int dim=model->dim_off; const int tdim = model->dim;
	double *const phitot = model->phitot;
	Node *r, *rn; double *p, *q, *newmu, *rdkn; int nx, dx, jid, docID, *idkn; long mx, nr_rs;
	float const BETA = model->BETA; float const ALPHA = model->ALPHA; float const WBETA = model->WBETA;
	double *phitot2 = new double[dim];
	double *tpmu = new double[dim];

	double mutot, tpmutot;
	double *const rdk = model->rdk;
	int *const idk = model->idk;

	while(true) {
		jid = scheduler->get_job();
		rn = TrG->GMS[jid]->M; nr_rs = TrG->GMS[jid]->nr_rs;
		rdkn = rdk + rn->did*dim;
		idkn = idk + rn->did*dim;

		docID = -1;
		double rrate = model->rrate;
					
		for(mx = 0; mx < dim; mx++) { 
			phitot2[mx] = phitot[mx];
		} 
		
		for(mx = 0; mx < nr_rs; mx++) {
			r = rn; rn++;
			float xi = r->rate;
			p = Phi + r->wid*dim;
			q = Theta + r->did*dim;
			newmu = mu[jid] + mx*dim;
			if(r->wid != docID) {
				rdkn = rdk + r->did*dim;
				idkn = idk + r->did*dim;
				docID = r->wid;
			}

			mutot = 0.0;
			tpmutot = 0.0;
			for(nx = 0; nx < (int)(tdim*rrate); nx ++) {
				dx = idkn[nx];
				tpmu[dx] = xi*newmu[dx];
				q[dx] -= tpmu[dx];
				if(q[dx] < 0) q[dx] = 0;
				p[dx] -= tpmu[dx];
				if(p[dx] < 0) p[dx] = 0;
				phitot2[dx] -= tpmu[dx];

				tpmu[dx] = (p[dx] + BETA)/(phitot2[dx] + WBETA)*(q[dx] + ALPHA);
				tpmutot += tpmu[dx];
				mutot += newmu[dx];
			}

			tpmutot = tpmutot/mutot;
			for(nx = 0; nx < (int)(tdim*rrate); nx ++) {
				dx = idkn[nx];
				tpmu[dx] = tpmu[dx]/tpmutot;
				rdkn[dx] += fabs(tpmu[dx] - newmu[dx]);
				newmu[dx] = tpmu[dx];
				tpmu[dx] = xi*tpmu[dx];
				p[dx] += tpmu[dx];
				q[dx] += tpmu[dx];
				phitot2[dx] += tpmu[dx];
			}
		}

		scheduler->put_job(jid);
		//scheduler->complete_on();
		scheduler->pause();
		if(scheduler->is_terminated()) break;
	}
	
	delete[] tpmu;
	delete[] phitot2;
}

void pem(GridMatrix *TrG, oModel *model, Monitor *monitor) {
	if(!model->showfg) printf("Starts!\n"); fflush(stdout);
	int iter=1;
	std::vector<std::thread> threads;
	Clock clock; clock.tic(); 

	Scheduler *scheduler = new Scheduler(model->nr_gdbs,model->nr_gwbs,model->nr_thrs);

	for(int tx=0; tx<model->nr_thrs; tx++) 
		threads.push_back(std::thread(emStep,TrG,model,scheduler,tx));

	if(!model->showfg) monitor->print_header(true);
	while(iter<=model->iter) {
		if(scheduler->get_total_jobs() >= model->nr_gdbs*model->nr_gwbs) {
			scheduler->pause_pem();
			model->updateParameter();

			float iter_time = clock.toc();
			
			if(EN_SHOW_SCHED) scheduler->show();
			monitor->show(iter, iter_time, model->showfg, 10, model, TrG);

			iter++; clock.tic();
			scheduler->resume();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	scheduler->terminate();

	//printf("Waiting for all threads terminate..."); fflush(stdout); clock.tic();
	for(auto it=threads.begin(); it!=threads.end(); it++)
		it->join(); 
	delete scheduler;
	//printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void train(int argc, char **argv) {
	long sum_count = 0;
	int par_size = 0;
	oModel *model = new oModel;
	Monitor *monitor = new Monitor;
	TrainOption *option = new TrainOption(argc, argv, model, monitor);
	Matrix *Tr; GridMatrix *TrG;
	
	Tr = new Matrix(option->tr_path);
	par_size = ceil((double)(Tr->nr_ds)/(model->partition_num));
	delete Tr;

	for(int ix = 0; ix < model->partition_num; ix ++) {
		Tr = new Matrix(option->tr_path, sum_count, par_size);
		if(Tr->nr_ds > 0)
			sum_count += Tr->nr_rs;
		else {
			printf("Stream Reading End!\n");
			break;
		}

		model->initialize(Tr, ix);

		if(model->en_rand_shuffle) {
			model->gen_rand_map();
			model->shuffle();
		}
		
		TrG = new GridMatrix(Tr,model->map_df,model->map_wf,model->nr_gdbs,model->nr_gwbs,model->nr_thrs);
		delete Tr;
	
		model->initData(TrG);
	
		pem(TrG,model,monitor);
	
		if(model->en_rand_shuffle) model->inv_shuffle();
		
		delete TrG;
		
		predict(model, monitor, option->te_path);
	}
	model->write(option->model_path);

	delete model; delete monitor; delete option;
}
