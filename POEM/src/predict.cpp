#include "poem.h"

void pred_te(GridMatrix *TeG, oModel *model, Scheduler *scheduler, int tid) {
	double *const Phi=model->Phi, *const Theta=model->Theta; 
	const int dim=model->dim_off ; const int tdim = model->dim;
	double *const phitot=model->phitot;
	Node *rn; double *pn, *qn; int dx, jid; long mx, nr_rs;
	float const BETA = model->BETA;
	float const ALPHA = model->ALPHA;
	float const WBETA = model->WBETA;

	double *newmu;
	double **const mu = model->mymu;
	double *phitot2 = new double[dim];	
	
	while(true) {
		jid = scheduler->get_job();
		rn = TeG->GMS[jid]->M; nr_rs = TeG->GMS[jid]->nr_rs; pn = Phi + rn->wid*dim; qn = Theta + rn->did*dim;
		float xi = rn->rate;
		for(mx = 0; mx < dim; mx++) { 
			phitot2[mx] = phitot[mx];
		}
		
		for(mx=0; mx<nr_rs; mx++) {
			newmu = mu[jid] + mx*dim;
			xi = rn->rate;
			pn = Phi + rn->wid*dim;
			qn = Theta + rn->did*dim;
		
			double mutot = 0.0;
			for(dx = 0; dx < tdim; dx ++) {
				qn[dx] = qn[dx] - xi*newmu[dx];
				if(qn[dx] < 0) qn[dx] = 0;
				newmu[dx] = (pn[dx] + BETA)/(phitot2[dx] + WBETA)*(qn[dx] + ALPHA);
				mutot += newmu[dx];
			}

			for(dx = 0; dx < tdim; dx ++) {
				newmu[dx] /= mutot;
				qn[dx] += newmu[dx]*xi;
			}
			rn++;
		}
		
		scheduler->put_job(jid);
		scheduler->pause();
		if(scheduler->is_terminated()) break;
	}
}

void predict(GridMatrix *TeG, oModel *model, Monitor *monitor) {
	//printf("Predict Starts!\n"); fflush(stdout);

	int iter=1;
	std::vector<std::thread> threads; Clock clock; clock.tic(); 

	Scheduler *scheduler = new Scheduler(model->nr_gdbs,model->nr_gwbs,model->nr_thrs);
	for(int tx=0; tx<model->nr_thrs; tx++) 
		threads.push_back(std::thread(pred_te, TeG, model, scheduler, tx));

	while(iter<=10) {
		if(scheduler->get_total_jobs() >= model->nr_gdbs*model->nr_gwbs) {
			scheduler->pause_pem();
			
			//model->updateParameter_te();

			float iter_time = clock.toc(); 

			if(EN_SHOW_SCHED) scheduler->show();
			
			monitor->show(iter, iter_time, -2, 10, model, TeG);
			
			iter++; clock.tic();
			scheduler->resume();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	scheduler->terminate();

	//Waiting for all threads terminate 
	for(auto it=threads.begin(); it!=threads.end(); it++)
		it->join();
	delete scheduler;
}

void predict(oModel *model, Monitor *monitor, char *te_path) {
	Matrix *Te = new Matrix(te_path);
	GridMatrix *TeG;

	model->initialize_te(Te);

	if(model->en_rand_shuffle) { 
		model->gen_rand_map(); model->shuffle();
	}

	TeG = new GridMatrix(Te,model->map_df,model->map_wf,model->nr_gdbs,model->nr_gwbs,model->nr_thrs);
	
	model->initData_te(TeG);

	predict(TeG, model, monitor);
	
	delete Te; delete TeG;
}
