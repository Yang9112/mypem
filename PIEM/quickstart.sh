#!/bin/sh

set -e

./pem convert ../dataset/cora cora.tr.bin
./pem convert ../dataset/cora cora.te.bin
./pem train --rand-shuffle --tr-perp -t 100 -s 10 -k 100 cora.tr.bin model
./pem predict cora.te.bin model
