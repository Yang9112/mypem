#include "apem.h"

aModel::aModel() {}
aModel::aModel(char *path) { read(path); }

void aModel::initialize(Matrix *Tr) {
	printf("Initializing model..."); fflush(stdout);
	Clock clock; clock.tic();

	int topic;

	nr_ds = Tr->nr_ds; nr_ws = Tr->nr_ws; nr_rs = Tr->nr_rs;
	dim_off = dim%4? (dim/4)*4+4 : dim;

	WBETA = BETA*nr_ws; JALPHA = ALPHA*dim;

	//initialzation the phi & theta
	Phi = new float[nr_ws*dim_off];
	Theta = new float[nr_ds*dim_off];
	mu = new float[dim_off];
	thetad = new float[nr_ds];
	phiw = new float[nr_ws];
	phitot = new double[dim_off];
	phitot2 = new double[dim_off];

	xitot = 0;
	for(int qx = 0; qx < nr_ds; qx++){
		thetad[qx] = 0;
		for(int dx = 0; dx < dim_off; dx++) {
			Theta[qx*dim_off + dx] = 0;
		}
	}
	for(int px = 0; px < nr_ws; px++) {
		phiw[px] = 0; 
		for(int dx = 0; dx < dim_off; dx++) {
			Phi[px*dim_off + dx] = 0;
		}
	}
	for(int ix = 0; ix < dim_off; ix++) {
		phitot[ix] = 0; phitot2[ix] = 0; mu[ix] = 0; 
	}

	//the id is starting from zero.
	srand48(0L);
	for(int mx = 0; mx < Tr->nr_rs; mx++) {
		float rating = Tr->M[mx].rate;
		thetad[Tr->M[mx].did] += rating;
		phiw[Tr->M[mx].wid] += rating;
		xitot += rating;
		
		topic = (int) (dim*drand48());
		Phi[Tr->M[mx].wid*dim_off + topic] += rating;
		Theta[Tr->M[mx].did*dim_off + topic] += rating;
		phitot[topic] += rating;
		mu[topic] += rating;
	}

	printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void aModel::initialize_te(Matrix *Te) {
	printf("Initializing test model..."); fflush(stdout);
	Clock clock; clock.tic();

	int topic, mx, dx;
	nr_ds = Te->nr_ds; nr_ws = Te->nr_ws; nr_rs = Te->nr_rs;
	WBETA = nr_ws*BETA; JALPHA = dim*ALPHA; xitot = 0.0;

	Theta = new float[nr_ds*dim_off];
	mu = new float[dim_off];
	thetad = new float[nr_ds];
	phitot = new double[dim_off];

	for(int qx = 0; qx < nr_ds; qx++) {
		thetad[qx] = 0;
		for(dx = 0; dx < dim_off; dx++) {
			Theta[qx*dim_off + dx] = 0;
		}
	}

	for(mx = 0; mx < dim_off; mx++) {
		phitot[mx] = 0; phitot2 = 0; mu[mx] = 0;
	}

	for(mx = 0; mx < nr_ws; mx++) {
		for(dx = 0; dx < dim; dx++)
			phitot[dx] += Phi[mx*dim_off + dx];
	}

	// random initialization
	srand48(0L);
	for(mx = 0; mx < nr_rs; mx++) {
		float xi = Te->M[mx].rate;
		xitot += xi;

		thetad[Te->M[mx].did] += xi;
		// pick a random topic 0..dim-1
		topic = (int)(dim*drand48());
		mu[topic] = 1.0;
		Theta[Te->M[mx].did*dim_off + topic] += xi;
	}

	printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void aModel::updateParameter() {
	for(int mx = 0; mx < dim; mx++) {
		phitot[mx] = 0;
		for(int px = 0; px < nr_ws; px++)
			phitot[mx] += Phi[px*dim_off + mx];
	}
}

aModel::~aModel() { 
	delete [] Phi; delete [] Theta;	delete [] phitot;
	delete [] thetad; delete [] phiw;
	delete [] Phi2; delete [] Theta2; delete [] phitot2;
	delete [] mu;
	delete [] map_df; delete [] map_db; delete [] map_wf; delete [] map_wb;
}

