#include "apem.h"

struct PredictOption {
	char *model_path, *test_path, *dst_path;
	PredictOption(int argc, char **argv);	
	static void exit_predict();
	~PredictOption();
};

PredictOption::PredictOption(int argc, char **argv) {
	if(argc!=5 && argc!=4) exit_predict();
	if(!strcmp(argv[1],"help")) exit_predict();
	model_path=argv[3], test_path=argv[2]; 
	if(argc==5) {
		dst_path = new char[strlen(argv[4])];
		sprintf(dst_path,"%s",argv[4]);
	}
	else {
		char *p = strrchr(argv[2],'/');
		if(p==NULL)
			p = argv[2];
		else
			++p;
		dst_path = new char[strlen(p)+5];
		sprintf(dst_path,"%s.out",p);
	}
}

PredictOption::~PredictOption() { delete [] dst_path; }

void PredictOption::exit_predict() {
	printf(
	"usage: pem predict binary_test_file model output\n"
	"\n"
	"Predict a test file from a model\n"
	); exit(1);
}

void pred_te(GridMatrix *TeG, aModel *model, Scheduler *scheduler, int tid) {
	double *const phitot=model->phitot;
	float *const Phi=model->Phi, *const Theta=model->Theta, *const thetad=model->thetad;
	const int dim=model->dim_off; const int tdim = model->dim;

	Node *r, *rn; float *p, *pn, *q, *qn; int dx, jid; long mx, nr_rs;

	float *mu = new float[dim], *myBeta = new float[dim];
	double *phitot3 = new double[dim];

	__m128 XMMWBeta = _mm_load1_ps(&model->WBETA), XMMAlpha = _mm_load1_ps(&model->ALPHA);

	for(mx = 0; mx < tdim; mx++) 
		phitot3[mx] = phitot[mx];

	for(int mx = 0; mx < dim; mx++) {
		if(mx < tdim)
			myBeta[mx] = model->BETA;
		else
			myBeta[mx] = 0;
	}

	while(true) {
		jid = scheduler->get_job();

		rn = TeG->GMS[jid]->M; nr_rs = TeG->GMS[jid]->nr_rs; 
		pn = Phi + rn->wid*dim; qn = Theta + rn->did*dim;
		
		for(mx=0; mx<nr_rs; mx++) {
			r = rn; rn++;
			p = pn; pn = Phi + rn->wid*dim;
			q = qn; qn = Theta + rn->did*dim;
			
			__m128 XMMr = _mm_load1_ps(&r->rate);
			__m128 XMMmutot = _mm_setzero_ps();
			for(dx = 0; dx < dim; dx += 4) {
				__m128 XMMq = _mm_load_ps(q + dx);
				__m128 XMMp = _mm_load_ps(p + dx);
				__m128 XMMthetad = _mm_load1_ps(thetad + r->did);
				XMMq = _mm_mul_ps(XMMq, _mm_div_ps(_mm_sub_ps(XMMthetad, XMMr), XMMthetad));
				_mm_store_ps(q + dx, XMMq);
				//q[dx]*= (thetad[r->did] - xi)/thetad[r->did];

				__m128d XMMphitot0 = _mm_load_pd(phitot3 + dx);
				__m128d XMMphitot1 = _mm_load_pd(phitot3 + dx + 2);
				__m128 XMMphitot = _mm_movelh_ps(_mm_cvtpd_ps(XMMphitot0), _mm_cvtpd_ps(XMMphitot1));
				
				__m128 XMMmu = _mm_load_ps(mu + dx);
				__m128 XMMBeta = _mm_load_ps(myBeta + dx);
				XMMmu = _mm_mul_ps(_mm_add_ps(XMMp, XMMBeta), _mm_add_ps(XMMq, XMMAlpha));
				XMMmu = _mm_div_ps(XMMmu, _mm_add_ps(XMMphitot, XMMWBeta));
				_mm_store_ps(mu + dx, XMMmu);
				//mu[dx] = (p[dx] + BETA)/(phitot3[dx] + WBETA)*(q[dx] + ALPHA);

				XMMmutot = _mm_add_ps(XMMmutot, XMMmu);
				//mutot += mu[dx];
			}

			XMMmutot = _mm_hadd_ps(XMMmutot, XMMmutot); XMMmutot = _mm_hadd_ps(XMMmutot, XMMmutot);

			for(dx = 0; dx < dim; dx += 4) {
				__m128 XMMmu = _mm_load_ps(mu + dx);
				__m128 XMMq = _mm_load_ps(q + dx);
				XMMmu = _mm_div_ps(XMMmu, XMMmutot);
				//mu[dx] /= mutot;

				XMMq = _mm_add_ps(XMMq, _mm_mul_ps(XMMmu, XMMr));
				_mm_store_ps(q + dx, XMMq);
				//q[dx] += mu[dx]*xi;
			}
		}
		
		scheduler->put_job(jid);
		scheduler->pause();
		if(scheduler->is_terminated()) break;

	}
	delete [] phitot3;
}

void predict(GridMatrix *TeG, aModel *model, Monitor *monitor) {
	printf("Predict Starts!\n"); fflush(stdout);

	int iter=1; 
	std::vector<std::thread> threads; Clock clock; clock.tic(); 

	Scheduler *scheduler = new Scheduler(model->nr_gdbs,model->nr_gwbs,model->nr_thrs);

	for(int tx=0; tx<model->nr_thrs; tx++)
		 threads.push_back(std::thread(pred_te,TeG,model,scheduler,tx));

	monitor->print_header();

	while(iter<=model->iter) {
		if(scheduler->get_total_jobs() >= model->nr_gdbs*model->nr_gwbs) {
			scheduler->pause_pem();	
			//scheduler->update_phitot_te(model);
			float iter_time = clock.toc();

			if(EN_SHOW_SCHED) scheduler->show();
			monitor->show(iter, iter_time, -1, 10, model, TeG);
			iter++; clock.tic();
			scheduler->resume();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	scheduler->terminate();

	printf("Waiting for all threads terminate..."); fflush(stdout); clock.tic();
	for(auto it=threads.begin(); it!=threads.end(); it++)
		it->join();
	delete scheduler;
	printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void predict(int argc, char **argv) {
	PredictOption *option = new PredictOption(argc,argv);

	aModel *model = new aModel(option->model_path);
	Matrix *Te = new Matrix(option->test_path);
	GridMatrix *TeG; Monitor *monitor = new Monitor;

	model->initialize_te(Te);

	if(model->en_rand_shuffle) { 
		model->gen_rand_map();
		model->shuffle();
	}

	TeG = new GridMatrix(Te,model->map_df,model->map_wf,model->nr_gdbs,model->nr_gwbs,model->nr_thrs);

	monitor->en_show_tr_perp = true;

	predict(TeG, model, monitor);

	delete option; delete model; delete Te; delete TeG; delete monitor;
}
