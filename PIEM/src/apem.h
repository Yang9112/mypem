#include "pem.h"

struct aModel:public Model {
	aModel();
	aModel(char *src);
	void initialize(Matrix *Tr);
	void initialize_te(Matrix *Tr);
	void updateParameter();
	~aModel();
};
