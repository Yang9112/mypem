PARALLEL EXPECTATION MAXIMIZATION ALGORITHM

This C++ code implements a new parallel expectation maximization algorithm.

You will need to install G++ in linux environment which can support the SSE
(Streaming SIMD Extensions).

===========================================================================
 			QUICK START OF PEM ALGORITHM
===========================================================================
Please Choose PEM1(synchronization) or PEM2(asynchronous) first.
usage:
To make file:
	make
To run:
	./pem convert train_file binary_train_file
	./pem convert test_file binary_test_file
	./pem train [Options] binary_train_file model
	./pem predict binary_test_file model
	(you can find the simple example in quickstart)
To clean:
	make clean


===========================================================================
				PEM COMMAND 
===========================================================================
**********************************************************************
1.		   	PRE-TREATMENT
**********************************************************************
you should convert both training and test data into binary file

usage: pem convert file_name binary_file_name

****************************
* Format of Input Data:		
*    docId   wordId  count
*    docId   wordId  count
*    ... ...
*    docId   wordId  count
****************************

**********************************************************************
2.			   TRAINING
**********************************************************************
You can train the pem model by the training set

usage: pem train [options] binary_train_file mode
options:
	-k <dimensions>: 	set the number of dimensions (default 10) 
	-t <iterations>: 	set the number of iterations (default 100) 
	-s <number of threads>: set the number of threads (default 4)  
	-blk <blocks>: 		set the number of blocks for parallel PEM (default 2s x 2s) 
				For example, if you want 3x4 blocks, then use '-blk 3x4' 
	-alpha			set the number of parameter(alpha) (default 0.01)
	-beta			set the number of parameter(beta) (default 0.01)
	--rand-shuffle, 
	--no-rand-shuffle: 	enable/disable random shuffle (default disabled)
				This options should be used when the data is imbalanced.
	--tr-perp,
	--no-tr-perp: 		enable/disable show perplexity on training data (default disabled)
				This option shows the estimated Perplexity on training data. 
				It also slows down the training procedure.

You can get any parameter you need in Model::write(tools/pem.cpp) by add 
	a write function.
For example:
	Phi, Theta, phitot or any other parameters.
**********************************************************************
3.			   PREDICT
**********************************************************************
Predict the test data by the model you trained
You can test the effect of the model by perplexity

usage: pem predict binary_test_file model

=============================================================================
The total program will run like this:
=============================================================================

Converting ../dataset/cora... done. 0.05
Writing cora.tr.bin... done. 0.00
Converting ../dataset/cora... done. 0.04
Writing cora.te.bin... done. 0.00
Reading from cora.tr.bin...done. 0.00
Initializing model...done. 0.01
Griding...done. 0.02
Starts!
iter       time    tr_perp
10         0.25    557.416
20         0.50    412.906
30         0.75    373.455
40         1.01    353.026
50         1.26    339.890
60         1.52    331.303
70         1.77    325.993
80         2.03    322.207
90         2.28    319.080
100        2.54    316.550
Waiting for all threads terminate...done. 0.00
Writing model...done. 0.00
Reading model...done. 0.00
Reading from cora.te.bin...done. 0.00
Initializing test model...done. 0.00
Griding...done. 0.03
Predict Starts!
iter       time    tr_perp
10         0.23    317.022
20         0.46    316.386
30         0.68    316.342
40         0.91    316.339
50         1.14    316.333
60         1.36    316.349
70         1.59    316.331
80         1.82    316.334
90         2.05    316.331
100        2.28    316.332
Waiting for all threads terminate...done. 0.00
