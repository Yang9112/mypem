#include "pem.h"

struct sModel:public Model {
	sModel();
	sModel(char *src);
	void initialize(Matrix *Tr);
	void initialize_te(Matrix *Tr);
	void updateParameter();
	void updateParameter_te();
	~sModel();
};
