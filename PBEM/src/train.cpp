#include "spem.h"

struct GridMatrix;

struct TrainOption {
	char *tr_path, *model_path;
	TrainOption(int argc, char **argv, sModel *model, Monitor *monitor);	
	static void exit_train();
	~TrainOption();
};

TrainOption::TrainOption(int argc, char **argv, sModel *model, Monitor *monitor) {

	model->dim=10, model->nr_thrs=4, model->iter=100, model->nr_gdbs=0, model->nr_gwbs=0;
	model->en_rand_shuffle=false;

	model->BETA = 0.01; model->ALPHA = 0.01;

	monitor->en_show_tr_perp=false;
	
	int i;
	for(i=2; i<argc; i++) {
		if(argv[i][0]!='-') break;
		if(i+1>=argc) exit_train();
		if(!strcmp(argv[i], "-k")) {
			model->dim = atoi(argv[++i]);
			if(model->dim<=0) { fprintf(stderr,"dimensions should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-t")) {
			model->iter = atoi(argv[++i]);
			if(model->iter<=0) { fprintf(stderr,"iterations should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-s")) {
			model->nr_thrs = atoi(argv[++i]);
			if(model->nr_thrs<=0) { fprintf(stderr,"number of threads should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-blk")) {
			char *p = strtok(argv[++i],"x");
			model->nr_gdbs = atoi(p);

			p = strtok(NULL,"x");
			model->nr_gwbs = atoi(p);

			if(model->nr_gdbs<=0 || model->nr_gwbs<=0) { fprintf(stderr,"number of blocks should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-alpha")) {
			model->ALPHA = atoi(argv[++i]);
			if(model->iter<=0) { fprintf(stderr,"the number of parameter(alpha) should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "-beta")) {
			model->BETA = atoi(argv[++i]);
			if(model->iter<=0) { fprintf(stderr,"the number of parameter(beta) should > 0\n"); exit(1); }
		}
		else if(!strcmp(argv[i], "--rand-shuffle")) model->en_rand_shuffle = true;
		else if(!strcmp(argv[i], "--no-rand-shuffle")) model->en_rand_shuffle = false;
		else if(!strcmp(argv[i], "--tr-perp")) monitor->en_show_tr_perp = true;
		else if(!strcmp(argv[i], "--no-tr-perp")) monitor->en_show_tr_perp = false;
		else { fprintf(stderr,"Invalid option: %s\n", argv[i]); exit_train(); }
	}

	if(model->nr_gdbs==0) (model->nr_gdbs) = 2*(model->nr_thrs);
	if(model->nr_gwbs==0) (model->nr_gwbs) = 2*(model->nr_thrs);

	if(i>=argc) exit_train();

	tr_path = argv[i++]; 
	
	if(i<argc) {
		model_path = new char[strlen(argv[i])+1];
		sprintf(model_path,"%s",argv[i]);
	}
	else {
		char *p = strrchr(argv[i-1],'/');
		if(p==NULL)
			p = argv[i-1];
		else
			++p;
		model_path = new char[strlen(p)+7];
		sprintf(model_path,"%s.model",p);
	}
} 

void TrainOption::exit_train() {
	printf(
	"usage: pem train [options] binary_train_file model\n"
	"\n"
	"options:\n" 
	"-k <dimensions>: set the number of dimensions (default 40)\n" 
	"-t <iterations>: set the number of iterations (default 40)\n" 
	"-s <number of threads>: set the number of threads (default 4)\n"  
	"-blk <blocks>: set the number of blocks for parallel PEM (default 2s x 2s)\n" 
	"	For example, if you want 3x4 blocks, then use '-blk 3x4'\n" 
	"-alpha: set the number of parameter(alpha) (default 0.01)\n"
	"-beta: set the number of parameter(beta) (default 0.01)\n"
	"--rand-shuffle --no-rand-shuffle: enable / disable random suffle (default disabled)\n"
	"	This options should be used when the data is imbalanced.\n"
	"--tr-perp --no-tr-perp: enable / disable show perplexity on training data (default disabled)\n"
	"	This option shows the estimated Perplexity on training data. It also slows down the training procedure.\n"
	); 
	exit(1);
}
TrainOption::~TrainOption() { delete [] model_path; }

void emStep(GridMatrix *TrG, sModel *model, Scheduler *scheduler, int tid) {
	float *const Phi=model->Phi, *const Theta=model->Theta, *const Phi2 = model->Phi2, *const Theta2 = model->Theta2;
	const int dim=model->dim_off ; const int tdim = model->dim;
	double *const phitot = model->phitot;
	Node *r, *rn; float *p, *pn, *p2, *q, *qn, *q2; int dx, jid; long mx, nr_rs;
	float *mu = new float[dim], *myBeta = new float[dim];
	double *phitot2 = new double[dim];

	__m128 XMMWBeta = _mm_load1_ps(&model->WBETA), XMMAlpha = _mm_load1_ps(&model->ALPHA);
	//__m128d XMMl = _mm_setzero_pd();
	for(int mx = 0; mx < dim; mx++) {
		if(mx < tdim)
			myBeta[mx] = model->BETA;
		else
			myBeta[mx] = 0;
	}

	while(true) {
		jid = scheduler->get_job();
		rn = TrG->GMS[jid]->M; nr_rs = TrG->GMS[jid]->nr_rs;
		pn = Phi + rn->wid*dim; qn = Theta + rn->did*dim;

		for(mx = 0; mx < dim; mx++) { 
			phitot2[mx] = phitot[mx];
		}

		//XMMl = _mm_setzero_pd();

		for(mx=0; mx<nr_rs; mx++) {
			r = rn; rn++;
			p = pn; pn = Phi + rn->wid*dim;
			q = qn;	qn = Theta + rn->did*dim;
			p2 = Phi2 + r->wid*dim;
			q2 = Theta2 + r->did*dim;
			__m128 XMMr = _mm_load1_ps(&r->rate);
			__m128 XMMmutot = _mm_setzero_ps();
			__m128 XMMBeta = _mm_load_ps(myBeta);

			for(dx = 0; dx < dim - 3; dx += 4) {
				__m128 XMMp = _mm_load_ps(p + dx);
				__m128 XMMq = _mm_load_ps(q + dx);
				__m128 XMMmu = _mm_load_ps(mu + dx);

				__m128 XMMphitot = _mm_cvtpd_ps(_mm_load_pd(phitot2 + dx));
				__m128 XMMphitot1 = _mm_cvtpd_ps(_mm_load_pd(phitot2 + dx + 2));
				
				XMMphitot = _mm_movelh_ps(XMMphitot, XMMphitot1);
				XMMphitot = _mm_add_ps(XMMWBeta, XMMphitot);
				XMMmu = _mm_mul_ps(_mm_add_ps(XMMp, XMMBeta), _mm_add_ps(XMMq, XMMAlpha));
				XMMmu = _mm_div_ps(XMMmu, XMMphitot);

				XMMmutot = _mm_add_ps(XMMmutot, XMMmu);
				_mm_store_ps(mu + dx, XMMmu);
				//mu[dx] = (p[dx] + BETA)/(phitot2[dx] + WBETA)*(q[dx] + ALPHA);
				//mutot += mu[dx];
			}
			
			for(; dx < dim; dx += 4) {
				__m128 XMMp = _mm_load_ps(p + dx);
				__m128 XMMq = _mm_load_ps(q + dx);
				__m128 XMMmu = _mm_load_ps(mu + dx);
				
				__m128 XMMBeta = _mm_load_ps(myBeta + dx);
				__m128 XMMphitot = _mm_cvtpd_ps(_mm_load_pd(phitot2 + dx));
				__m128 XMMphitot2 = _mm_cvtpd_ps(_mm_load_pd(phitot2 + dx + 2));
				XMMphitot = _mm_movelh_ps(XMMphitot, XMMphitot2);
				XMMphitot = _mm_add_ps(XMMWBeta, XMMphitot);
				XMMmu = _mm_mul_ps(_mm_add_ps(XMMp, XMMBeta), _mm_add_ps(XMMq, XMMAlpha));
				XMMmu = _mm_div_ps(XMMmu, XMMphitot);

				XMMmutot = _mm_add_ps(XMMmutot, XMMmu);
				_mm_store_ps(mu + dx, XMMmu);
				//mu[dx] = (p[dx] + BETA)/(phitot2[dx] + WBETA)*(q[dx] + ALPHA);
				//mutot += mu[dx];
			}

			XMMmutot = _mm_hadd_ps(XMMmutot, XMMmutot); XMMmutot = _mm_hadd_ps(XMMmutot, XMMmutot);

			for(dx = 0; dx < dim; dx += 4) {
				__m128 XMMmu = _mm_load_ps(mu + dx);
				__m128 XMMp2 = _mm_load_ps(p2 + dx);
				__m128 XMMq2 = _mm_load_ps(q2 + dx);
				XMMmu = _mm_div_ps(XMMmu, XMMmutot);
				//mu[dx] /= mutot;

				XMMp2 = _mm_add_ps(XMMp2, _mm_mul_ps(XMMmu, XMMr));
				XMMq2 = _mm_add_ps(XMMq2, _mm_mul_ps(XMMmu, XMMr));
				_mm_store_ps(p2 + dx, XMMp2);
				_mm_store_ps(q2 + dx, XMMq2);
				//q2[dx] += mu[dx]*xi;
				//p2[dx] += mu[dx]*xi;
			}
		}
		scheduler->put_job(jid);

		//scheduler->complete_on();
		scheduler->pause();
		if(scheduler->is_terminated()) break;
	}
}

void pem(GridMatrix *TrG, sModel *model, Monitor *monitor) {
	printf("Starts!\n"); fflush(stdout);
	int iter=1;
	std::vector<std::thread> threads;
	Clock clock; clock.tic(); 

	Scheduler *scheduler = new Scheduler(model->nr_gdbs,model->nr_gwbs,model->nr_thrs);

	for(int tx=0; tx<model->nr_thrs; tx++) 
		threads.push_back(std::thread(emStep,TrG,model,scheduler,tx));

	monitor->print_header();
	while(iter<=model->iter) {
		if(scheduler->get_total_jobs() >= model->nr_gdbs*model->nr_gwbs) {
			scheduler->pause_pem();
			model->updateParameter();

			float iter_time = clock.toc();

			if(EN_SHOW_SCHED) scheduler->show();

			monitor->show(iter, iter_time, -1, 10, model, TrG);

			iter++; clock.tic();
			scheduler->resume();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	scheduler->terminate();

	printf("Waiting for all threads terminate..."); fflush(stdout); clock.tic();
	
	for(auto it=threads.begin(); it!=threads.end(); it++)
		it->join(); 
	delete scheduler;
	printf("done. %.2f\n", clock.toc()); fflush(stdout);
}

void train(int argc, char **argv) {
	sModel *model = new sModel;
	Monitor *monitor = new Monitor;
	TrainOption *option = new TrainOption(argc, argv, model, monitor);
	Matrix *Tr; GridMatrix *TrG;

	Tr = new Matrix(option->tr_path);
	model->initialize(Tr);

	if(model->en_rand_shuffle) {
		model->gen_rand_map();
		model->shuffle();
	}

	TrG = new GridMatrix(Tr,model->map_df,model->map_wf,model->nr_gdbs,model->nr_gwbs,model->nr_thrs);
	delete Tr;

	pem(TrG,model,monitor);
	
	if(model->en_rand_shuffle) model->inv_shuffle();

	model->write(option->model_path);

	delete model; delete monitor; delete option; delete TrG; 
}
