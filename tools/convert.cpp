#include "pem.h"

struct ConvertOption {
	char *src, *dst;
	ConvertOption(int argc, char **argv);	
	static void exit_convert();
	~ConvertOption();
};

ConvertOption::ConvertOption(int argc, char **argv) {
	if(argc!=3 && argc!=4) exit_convert();

	src = argv[2];
	if(argc==4) {
		dst = new char[strlen(argv[3])+1];
		sprintf(dst,"%s",argv[3]); 
	}
	else {
		char *p = strrchr(argv[2],'/');
		if(p==NULL) p = argv[2];
		else p++;
		dst = new char[strlen(p)+5];
		sprintf(dst,"%s.bin",p);
	}
}

void ConvertOption::exit_convert() {
	printf(
		"usage: pem convert text_file binary_file\n"
		"\n"
		"Convert a text file to a binary file\n"
	); 
	exit(1);
}

ConvertOption::~ConvertOption() { delete[] dst; }

void convert(char *src_path, char *dst_path) {
	printf("Converting %s... ", src_path); fflush(stdout);
	Clock clock; clock.tic();

	int did, wid, nr_ds=0, nr_ws=0, nr_rs; float rate;

	FILE *f = fopen(src_path, "r"); if(!f) exit_file_error(src_path);
	std::vector<Node> rs; 

	while(fscanf(f,"%d %d %f\n",&did,&wid,&rate)!=EOF) {
		if(did+1 > nr_ds) nr_ds = did+1;
		if(wid+1 > nr_ws) nr_ws = wid+1;
		
		Node r;
		r.did=did, r.wid=wid, r.rate=rate;
		rs.push_back(r);
	}
	nr_rs = rs.size(); fclose(f);

	Matrix *R = new Matrix(nr_rs,nr_ds,nr_ws);

	for(auto it=rs.begin(); it!=rs.end(); it++) 
		R->M[it-rs.begin()] = (*it); 

	printf("done. %.2f\n", clock.toc()); fflush(stdout);

	R->write(dst_path);
	delete R;
}

void convert(int argc, char **argv) {
	ConvertOption *option = new ConvertOption(argc,argv);

	convert(option->src,option->dst);

	delete option;
}
