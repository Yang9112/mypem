#include "pem.h"

struct ViewOption {
	char *src;
	ViewOption(int argc, char **argv);
	static void exit_view();
};

void ViewOption::exit_view() {
	printf(
	"usage: pem view file\n"
	"\n"
	"View info in a binary data or model file\n"
	); exit(1);
}

ViewOption::ViewOption(int argc, char **argv) {
	if(argc!=3) exit_view();
	if(!strcmp(argv[1],"help")) exit_view();
	src = argv[2];
	FILE *f = fopen(src, "rb"); if(!f) exit_file_error(src); fclose(f);
}

void view_data(FILE *f) {
	Matrix *R = new Matrix; fseek(f,0,SEEK_SET);
	R->read_meta(f);
	printf("number of words = %d\n", R->nr_ds);
	printf("number of documents = %d\n", R->nr_ws);
	printf("number of ratings = %ld\n", R->nr_rs);
}

void view_model(FILE *f) {
	Model *model = new Model; fseek(f,0,SEEK_SET);
	model->read_meta(f);
	printf("dimensions = %d\n", model->dim);
	printf("iterations = %d\n", model->iter);
	printf("random shuffle = %d\n", (int)model->en_rand_shuffle);
}

void view(int argc, char **argv) {
	ViewOption option(argc,argv);

	FILE *f = fopen(option.src, "rb"); int type;

	fread(&type,sizeof(int),1,f);

	if(type==DATA) view_data(f);
	else if(type==MODEL) view_model(f);
	else fprintf(stderr,"Invalid file type.\n");

	fclose(f);
}