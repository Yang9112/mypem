#pragma GCC diagnostic ignored "-Wunused-result" 
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <csignal>
#include <ctime>
#include <cstring>
#include <climits>
#include <cfloat>
#include <random>
#include <numeric>
#include <algorithm>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <pmmintrin.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <iostream>
#include <sys/time.h>

#define DATAVER 1
#define MODELVER 1

#define EN_SHOW_SCHED false
#define EN_SHOW_GRID false

#define flag fprintf(stderr, "LINE: %d\n", __LINE__)

enum FileType {DATA,MODEL};
void convert(int argc, char **argv);
void train(int argc, char **argv);
void predict(int argc, char **argv);
void view(int argc, char **argv);
void exit_file_error(char *path);
void exit_file_ver(float ver);

struct Clock {
	struct timeval begin, end;
	void tic();
	float toc();
};

struct Node {
	int did, wid; 
	float rate;
};

struct Matrix {
	int nr_ds, nr_ws;
	long cur_seek;
	long nr_rs;
	Node *M;
	Matrix();
	Matrix(long nr_rs, int nr_ds, int nr_ws);
	Matrix(char *path);
	Matrix(char *path, long start_order, long stream_size);
	Matrix(char *path, int *map_u, int *map_i);
	void read_meta(FILE *f);
	void read(char *path);
	void read(char *path, long start_ordre, long stream_size);
	void write(char *path);
	void sort();
	static bool sort_did_wid(Node lhs, Node rhs);
	~Matrix();
};

struct Model {
	long nr_rs;
	int nr_ds, nr_ws, dim, dim_off, nr_thrs, iter, nr_gdbs, nr_gwbs, *map_df, *map_db, *map_wf, *map_wb;
	float *Theta, *Phi, *Theta2, *Phi2, *mu, *thetad, *phiw, BETA, ALPHA, WBETA, JALPHA;
	double *phitot, *phitot2, xitot;
	bool en_rand_shuffle;
	Model() {};
	Model(char *src) {};
	void read_meta(FILE *f);
	virtual void read(char *path);
	virtual void write(char *path);
	void gen_rand_map();
	virtual void shuffle();
	virtual void inv_shuffle();
	virtual double get_perp(Matrix *R);
	virtual void initialize(Matrix *Tr) {};
	virtual void initialize_te(Matrix *Tr) {};
	virtual void updateParameter() {};
	virtual void updateParameter_te() {};
	virtual ~Model() {};
};

struct GridMatrix {
	int nr_gdbs, nr_gwbs;
	long nr_rs;
	Matrix **GMS;
	GridMatrix(Matrix *G, int *map_u, int *map_i, int nr_gdbs, int nr_gwbs, int nr_thrs);
	static void sort_ratings(Matrix *M, std::mutex *mtx, int *nr_thrs);
	~GridMatrix();
};

struct Monitor {
	int iter;
	float tr_time, te_time;
	bool en_show_tr_perp;
	bool en_show_te_perp;

	Monitor();
	void print_header(bool fg = false);
	void show(int it, float iter_time, int docsID, int interval, Model *model, GridMatrix *TG);
	~Monitor();
};

class Scheduler {
	int *nr_jts, *order, nr_gdbs, nr_gwbs, nr_thrs, total_jobs, nr_paused_thrs;
	bool *blocked_u, *blocked_i, *block_x, paused, terminated;
	std::mutex mtx;
	bool all_paused();
public:
	Scheduler(int nr_gdbs, int nr_gwbs, int nr_thrs);
	int get_job();
	void put_job(int jid);
	int get_total_jobs();
	void pause_pem();
	void pause();
	void resume();
	void terminate();
	bool is_terminated();
	void show();
	~Scheduler();
};
