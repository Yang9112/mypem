#include "pem.h"

void exit_file_error(char *path) {
	fprintf(stderr,"\nError: Invalid file name %s.\n", path);
	exit(1);
}

void exit_file_ver(float current_ver, float file_ver) { 
	fprintf(stderr,"\nError: Inconsistent file version.\n");
	fprintf(stderr,"current version:%.2f	file version:%.2f\n",current_ver,file_ver);
	exit(1); 
}

void Clock::tic() {
	gettimeofday(&begin,NULL);
}

float Clock::toc() {
	gettimeofday(&end,NULL);
	return ((float)(end.tv_sec-begin.tv_sec) + (float)(end.tv_usec-begin.tv_usec)/1000000);
}

Matrix::Matrix() { }

Matrix::Matrix(long nr_rs, int nr_ds, int nr_ws) : nr_ds(nr_ds), nr_ws(nr_ws), nr_rs(nr_rs) {
	M = new Node[nr_rs];
}

Matrix::Matrix(char *path) { 
	read(path);
	cur_seek = 0;
}

Matrix::Matrix(char *path, long start_order, long stream_size) {
	read(path, start_order, stream_size);
}

Matrix::Matrix(char *path, int *map_u, int *map_i) {
	read(path); 
	for(long rx=0; rx<nr_rs; rx++) {
		M[rx].did = map_u[M[rx].did];
		M[rx].wid = map_i[M[rx].wid];
	}
}

void Matrix::read_meta(FILE *f) {
	int type; float ver;

	fread(&type,sizeof(int),1,f); if(type!=(int)DATA) { 
		fprintf(stderr,"Error: It is not a data file.\n");
		exit(1);
	}
	fread(&ver,sizeof(float),1,f);
	if(ver!=(float)DATAVER)
		exit_file_ver(DATAVER,ver);
	fread(this,sizeof(Matrix),1,f);
	
	this->M = NULL;
}

void Matrix::read(char *path) {
	//printf("Reading from %s...",path); fflush(stdout);
	Clock clock; clock.tic();
	
	FILE *f = fopen(path, "rb");
	if(!f) exit_file_error(path);
	
	read_meta(f);
	M = new Node[nr_rs];
	
	fread(M,sizeof(Node),nr_rs,f);
	fclose(f);
	
	//printf("done. %.2f\n",clock.toc()); fflush(stdout);
}

void Matrix::read(char *path, long start_order, long stream_size) {
	int ix = 0, doc_num = 0;
	int cur_doc = 0, start_doc;
	FILE *f = fopen(path, "rb");
	if(!f) exit_file_error(path);
	
	read_meta(f);
	M = (Node *) malloc(sizeof(Node));
	//M = new Node[1];	
	
	fseek(f, start_order*sizeof(Node), 1);
	
	if(!feof(f)) {
		fread(M, sizeof(Node), 1, f);
		nr_ds = 0;
	}
	else
		return;
	start_doc = M[0].did;
	M[0].did = 0;
	doc_num++;

	while(!feof(f)) {
		ix++;
		M = (Node *)realloc(M, (ix+1)*sizeof(Node));
		fread(M + ix, sizeof(Node), 1, f);
		M[ix].did = M[ix].did - start_doc;
		if(doc_num < stream_size) {
			if(cur_doc != M[ix].did) {
				doc_num++;
				cur_doc = M[ix].did;
			}
		}
		else
			break;
	}

	nr_rs = ix;
	nr_ds = M[ix-1].did - M[0].did + 1;
	fclose(f);
}

void Matrix::write(char *path) {
	printf("Writing %s... ",path);
	fflush(stdout);
	Clock clock; clock.tic();
	
	FILE *f = fopen(path,"wb");
	if(!f) exit_file_error(path);
	
	float ver = (float)DATAVER;
	int file_type = DATA;
	
	fwrite(&file_type,sizeof(int),1,f);
	fwrite(&ver,sizeof(float),1,f);
	fwrite(this,sizeof(Matrix),1,f);
	fwrite(M,sizeof(Node),nr_rs,f);
	
	fclose(f);
	printf("done. %.2f\n",clock.toc()); fflush(stdout);
}

void Matrix::sort() { std::sort(M,M+nr_rs,Matrix::sort_did_wid); }
bool Matrix::sort_did_wid(Node lhs, Node rhs) {
	if(lhs.did!=rhs.did)
		return lhs.did < rhs.did;
	else
		return lhs.wid < rhs.wid;
}
Matrix::~Matrix() { delete [] M; }

void Model::read_meta(FILE *f) {
	int type; float ver;

	fread(&type,sizeof(int),1,f);
	if(type!=(int)MODEL) { 
		fprintf(stderr,"Error: It is not a model file.\n");
		exit(1);
	}

	fread(&ver, sizeof(float), 1, f);
	if(ver!=(float)MODELVER)
		exit_file_ver(MODELVER,ver);

	fread(this,sizeof(Model),1,f);
	this->Phi = NULL; this->Theta = NULL;
	this->Phi2 = NULL; this->Theta2 = NULL;
	this->phitot = NULL; this->phitot2 = NULL;
	this->mu = NULL; this->thetad = NULL; this->phiw = NULL;
	this->map_wf = NULL; this->map_wb = NULL; this->map_df = NULL; this->map_db = NULL;
}

void Model::read(char *path) {
	printf("Reading model..."); fflush(stdout);
	Clock clock; clock.tic();
	FILE *f = fopen(path,"rb"); if(!f) exit_file_error(path);
	read_meta(f);

	Phi = new float[nr_ws*dim_off];
	fread(Phi, sizeof(float), nr_ws*dim_off, f);
	phiw = new float[nr_ws];
	fread(phiw, sizeof(float), nr_ws, f);
	phitot = new double[dim_off];
	fread(phitot, sizeof(double), dim_off, f);
	//Theta = new float[nr_ds*dim_off];
	//fread(Theta, sizeof(float), nr_ds*dim_off, f);

	if(en_rand_shuffle) {
		map_df = new int[nr_ds]; 
		map_db = new int[nr_ds];
		map_wf = new int[nr_ws];
		map_wb = new int[nr_ws]; 
		fread(map_df, sizeof(int), nr_ds, f);
		fread(map_db, sizeof(int), nr_ds, f);
		fread(map_wf, sizeof(int), nr_ws, f);
		fread(map_wb, sizeof(int), nr_ws, f);
	}
	fclose(f);
	printf("done. %.2f\n",clock.toc()); fflush(stdout);
}

void Model::write(char *path) {
	printf("Writing model..."); fflush(stdout);
	Clock clock; clock.tic();
	FILE *f = fopen(path, "wb"); 
	if(!f) exit_file_error(path);
	
	float ver = (float)MODELVER;
	int file_type = MODEL;
	
	fwrite(&file_type,sizeof(int),1,f);
	fwrite(&ver,sizeof(float),1,f);
	fwrite(this,sizeof(Model),1,f);
	fwrite(Phi,sizeof(float),nr_ws*dim_off,f);
	fwrite(phiw, sizeof(float), nr_ws, f);
	fwrite(phitot, sizeof(double), dim_off, f);
	//fwrite(Theta,sizeof(float),nr_ds*dim_off,f);
	if(en_rand_shuffle) {
		fwrite(map_df, sizeof(int), nr_ds, f);
		fwrite(map_db, sizeof(int), nr_ds, f);
		fwrite(map_wf, sizeof(int), nr_ws, f);
		fwrite(map_wb, sizeof(int), nr_ws, f);
	}
	fclose(f);
	printf("done. %.2f\n", clock.toc()); fflush(stdout);
}
void Model::gen_rand_map() {
	map_df = new int[nr_ds];
	map_db = new int[nr_ds];
	map_wf = new int[nr_ws];
	map_wb = new int[nr_ws];

	for(int ix=0; ix<nr_ds; ix++)
		map_df[ix] = ix; 
	for(int ix=0; ix<nr_ws; ix++)
		map_wf[ix] = ix;

	std::random_shuffle(map_df,map_df+nr_ds);
	std::random_shuffle(map_wf,map_wf+nr_ws);

	for(int ix=0; ix<nr_ds; ix++) 
		map_db[map_df[ix]] = ix;
	for(int ix=0; ix<nr_ws; ix++)
		map_wb[map_wf[ix]] = ix;
}

void Model::shuffle() {
	float *Theta1 = new float[nr_ds*dim_off];
	float *Phi1 = new float[nr_ws*dim_off];
	float *thetad1 = new float[nr_ds];
	float *phiw1 = new float[nr_ws];

	for(int px=0; px<nr_ds; px++) {
		std::copy(&Theta[px*dim_off],&Theta[px*dim_off + dim_off],&Theta1[map_df[px] * dim_off]);
		thetad1[map_df[px]] = thetad[px];
	}
	for(int qx=0; qx<nr_ws; qx++) {
		std::copy(&Phi[qx*dim_off],&Phi[qx*dim_off + dim_off],&Phi1[map_wf[qx] * dim_off]);
		phiw1[map_wf[qx]] = phiw[qx];
	}

	delete [] Phi; delete [] Theta; 
	delete [] phiw; delete [] thetad;

	Phi = Phi1; Theta = Theta1;
	phiw = phiw1; thetad = thetad1;
}

void Model::inv_shuffle() {
	float *Theta1 = new float[nr_ds*dim_off]; 
	float *Phi1 = new float[nr_ws*dim_off];
	float *thetad1 = new float[nr_ds];
	float *phiw1 = new float[nr_ws];

	for(int px=0; px<nr_ds; px++) {
		std::copy(&Theta[px*dim_off],&Theta[px*dim_off + dim_off],&Theta1[map_db[px]*dim_off]);
		thetad1[map_db[px]] = thetad[px];
	}
	for(int qx=0; qx<nr_ws; qx++) {
		std::copy(&Phi[qx*dim_off],&Phi[qx*dim_off + dim_off],&Phi1[map_wb[qx]*dim_off]);
		phiw1[map_wb[qx]] = phiw[qx];
	}

	delete [] Phi; delete [] Theta;
	delete [] phiw; delete [] thetad;

	Phi = Phi1; Theta = Theta1; 
	phiw = phiw1; thetad = thetad1;
}

double Model::get_perp(Matrix *R) {
	double perp = 0.0, mutot;
	long int nr_rs = R->nr_rs;
	Node *rn = R->M;
 	
	for(long mx = 0; mx < nr_rs; mx++, rn++) {
		float xi = rn->rate;
		mutot = 0.0;
		for(int dx = 0; dx < dim; dx++) {
			mutot += (Phi[rn->wid*dim_off + dx] + BETA)/(phitot[dx] + WBETA)*
				(Theta[rn->did*dim_off + dx] + ALPHA)/(thetad[rn->did] + JALPHA);
		}
		perp -= (log(mutot)*xi);
	}

	return perp;
}

GridMatrix::GridMatrix(Matrix *R, int *map_u, int *map_i, int nr_gdbs, int nr_gwbs, int nr_thrs) : nr_gdbs(nr_gdbs), nr_gwbs(nr_gwbs) {
	
	//printf("Griding..."); fflush(stdout);

	Clock clock; clock.tic(); GMS = new Matrix*[nr_gdbs*nr_gwbs]; this->nr_rs = R->nr_rs; std::mutex mtx;
	int seg_u = (int)ceil(double(R->nr_ds)/nr_gdbs), seg_i = (int)ceil(double(R->nr_ws)/nr_gwbs);

	int r_map[nr_gdbs][nr_gwbs];
	for(int mx=0; mx<nr_gdbs; mx++)
		for(int nx=0; nx<nr_gwbs; nx++) 
			r_map[mx][nx] = 0;
	
	for(long rx=0; rx<R->nr_rs; rx++) { 
		int new_did = map_u? map_u[R->M[rx].did] : R->M[rx].did;
		int new_wid = map_i? map_i[R->M[rx].wid] : R->M[rx].wid;
		r_map[new_did/seg_u][new_wid/seg_i]++; 
	}

	//count the number of ratings per block.
	for(int mx=0; mx<nr_gdbs; mx++)
		for(int nx=0; nx<nr_gwbs; nx++)
			GMS[mx*nr_gwbs+nx] = new Matrix(r_map[mx][nx],-1,-1);

	for(int mx=0; mx<nr_gdbs; mx++) 
		for(int nx=0; nx<nr_gwbs; nx++)
			r_map[mx][nx] = 0; // Use r_map as index counter.

	for(long rx=0; rx<R->nr_rs; rx++) {
		int new_did = map_u? map_u[R->M[rx].did] : R->M[rx].did;
		int new_wid = map_i? map_i[R->M[rx].wid] : R->M[rx].wid;
		int thub = new_did/seg_u, thib = new_wid/seg_i;
		GMS[thub*nr_gwbs+thib]->M[r_map[thub][thib]] = R->M[rx];
		GMS[thub*nr_gwbs+thib]->M[r_map[thub][thib]].did = new_did;
		GMS[thub*nr_gwbs+thib]->M[r_map[thub][thib]++].wid = new_wid;
	}

	if(map_u) {
		int nr_alive_thrs = 0;
		for(int mx=0; mx < nr_gdbs*nr_gwbs; mx++) {
			while(nr_alive_thrs >= nr_thrs) { 
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				continue;
			}

			{ std::lock_guard<std::mutex> lock(mtx); nr_alive_thrs++; }
			
			std::thread worker = std::thread(GridMatrix::sort_ratings, GMS[mx], &mtx, &nr_alive_thrs); worker.detach();
		}
		while(nr_alive_thrs!=0) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			continue;
		}
	}

	//printf("done. %.2f\n", clock.toc()); fflush(stdout);

	if(EN_SHOW_GRID) {
		for(int mx=0; mx<nr_gdbs; mx++) {
			for(int nx=0; nx<nr_gwbs; nx++)
				printf("%7ld ", GMS[mx*nr_gwbs+nx]->nr_rs);
			printf("\n");
		}
		printf("\n");
	}
}

void GridMatrix::sort_ratings(Matrix *M, std::mutex *mtx, int *nr_thrs) {
	M->sort();
	std::lock_guard<std::mutex> lock(*mtx);
	(*nr_thrs)--;
}

GridMatrix::~GridMatrix() {
	for(int ix=0; ix<nr_gdbs*nr_gwbs; ix++)
		delete GMS[ix];
	delete GMS;
}


Monitor::Monitor() : iter(0), tr_time(0.0), te_time(0.0) { }

void Monitor::print_header(bool fg) {
	char output[1024];
	if(fg) {
		sprintf(output, "%-8s", "DocsID");
		sprintf(output+strlen(output), "%4s", "iter"); 
	}
	else
		sprintf(output, "%4s", "iter"); 
		
	sprintf(output+strlen(output), " %10s", "tr_time"); 
 
	if(en_show_tr_perp) sprintf(output+strlen(output), " %10s", "tr_perp"); 
	if(en_show_te_perp) sprintf(output+strlen(output), " %10s %10s", "te_time", "te_perp"); 
	printf("%s\n", output);
}

void Monitor::show(int it, float iter_time, int docsID, int interval, Model *model, GridMatrix *TG) {
	char output[1024]; 
	
	if(docsID >= 0) {
		tr_time += iter_time;
		if(it % interval == 0) {
			sprintf(output, "%-8d%-4d %10.2f", docsID, it, tr_time); 
			if(en_show_tr_perp) { 
				double perplexity = 0.0;
				for(int mx = 0; mx < model->nr_gdbs*model->nr_gwbs; mx++)
					perplexity += model->get_perp(TG->GMS[mx]);
				sprintf(output+strlen(output), " %10.3f", exp(perplexity/model->xitot)); 
			}

			printf("%s\n", output); fflush(stdout);  
		}
	}
	else if(docsID == -1) {
		tr_time += iter_time;
		if(it % interval == 0) {
			sprintf(output, "%-4d %10.2f", it, tr_time); 
			if(en_show_tr_perp) { 
				double perplexity = 0.0;
				for(int mx = 0; mx < model->nr_gdbs*model->nr_gwbs; mx++)
					perplexity += model->get_perp(TG->GMS[mx]);
				sprintf(output+strlen(output), " %10.3f", exp(perplexity/model->xitot)); 
			}

			printf("%s\n", output); fflush(stdout);
		}
	}
	else {
		te_time += iter_time;
		if(it % interval == 0 && en_show_te_perp) {
			if(en_show_tr_perp)
				sprintf(output, "%-34s %10.2f", "(Test Set)", te_time);  
			else
				sprintf(output, "%-23s %10.2f", "(Test Set)", te_time);  

			double perplexity = 0.0;
			for(int mx = 0; mx < model->nr_gdbs*model->nr_gwbs; mx++)
				perplexity += model->get_perp(TG->GMS[mx]);
			sprintf(output+strlen(output), " %10.3f", exp(perplexity/model->xitot)); 
			printf("%s\n", output); fflush(stdout);
		}
	}
}

Monitor::~Monitor() { }

Scheduler::Scheduler(int nr_gdbs, int nr_gwbs, int nr_thrs) : nr_gdbs(nr_gdbs), nr_gwbs(nr_gwbs),
				nr_thrs(nr_thrs), total_jobs(0), nr_paused_thrs(0), paused(false), terminated(false) {

	nr_jts = new int[nr_gdbs*nr_gwbs];
	order = new int[nr_gdbs*nr_gwbs];
	blocked_u = new bool[nr_gdbs];
	blocked_i = new bool[nr_gwbs];
	block_x = new bool[nr_gdbs*nr_gwbs];

	for(int mx=0; mx<nr_gdbs*nr_gwbs; mx++)
		nr_jts[mx]=0, order[mx]=mx;

	for(int mx=0; mx<nr_gdbs; mx++)
		blocked_u[mx] = false; 
	for(int mx=0; mx<nr_gwbs; mx++)
		blocked_i[mx] = false; 
	for(int mx=0; mx<nr_gwbs*nr_gdbs; mx++)
		block_x[mx] = true;
}

int Scheduler::get_job() {

	//int jid=-1, ts=INT_MAX;
	int jid=-1;
	while(true) {
		{
			std::lock_guard<std::mutex> lock(mtx);
			{
				for(int mx=0; mx<nr_gdbs*nr_gwbs; mx++) {
					int nx = order[mx];
					if(blocked_u[nx/nr_gwbs] || blocked_i[nx%nr_gwbs] || !block_x[nx]) continue;
					else { jid = nx; break; }
					//if(nr_jts[nx]<ts) ts=nr_jts[nx], jid=nx;
				}
				block_x[jid] = false;
			}
		}
		if(jid!=-1) break;
		pause();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	{
		std::lock_guard<std::mutex> lock(mtx);
		blocked_u[jid/nr_gwbs]=true, blocked_i[jid%nr_gwbs]=true, nr_jts[jid]++;
	}
	return jid;
}

void Scheduler::put_job(int jid) { 
	{
		std::lock_guard<std::mutex> lock(mtx); 
   		blocked_u[jid/nr_gwbs]=false, blocked_i[jid%nr_gwbs]=false, total_jobs++;
	}
}

int Scheduler::get_total_jobs() { return total_jobs; }

void Scheduler::pause_pem() { 
	int total_block;
	do{
		total_block = 0;
		for(int mx = 0; mx < nr_gwbs*nr_gdbs; mx++) {
			std::lock_guard<std::mutex> lock(mtx);
			total_block += block_x[mx];
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}while(total_block);

	{
		std::lock_guard<std::mutex> lock(mtx);
		paused = true; 
	}

	while(!all_paused())
		std::this_thread::sleep_for(std::chrono::milliseconds(1));

	for(int mx=0; mx < nr_gwbs*nr_gdbs; mx++)
		block_x[order[mx]] = true;
}

void Scheduler::pause() { 
	{
		std::lock_guard<std::mutex> lock(mtx);
		if(!paused) return; 
	}
	{
		std::lock_guard<std::mutex> lock(mtx);
		nr_paused_thrs++;
	}
	while(paused) std::this_thread::sleep_for(std::chrono::milliseconds(1));
	{
		std::lock_guard<std::mutex> lock(mtx);
		nr_paused_thrs--;
	}
}

bool Scheduler::all_paused() { 
	std::lock_guard<std::mutex> lock(mtx);
	return (nr_paused_thrs==nr_thrs);
}

void Scheduler::resume() { 
	std::lock_guard<std::mutex> lock(mtx); 
	std::random_shuffle(order, order+nr_gdbs*nr_gwbs);
	paused = false;
	total_jobs = 0;
}

void Scheduler::terminate() { terminated = true; }

bool Scheduler::is_terminated() { return terminated; }

void Scheduler::show() {
	for(int mx=0; mx<nr_gdbs; mx++) {
		for(int nx=0; nx<nr_gwbs; nx++) 
			printf("%3d ", nr_jts[mx*nr_gwbs+nx]);
		printf("\n");
	}
	printf("\n"); fflush(stdout);
}

Scheduler::~Scheduler() { delete [] nr_jts; delete [] order; delete [] blocked_u; delete [] blocked_i; }
